from re import S

drinks = {
    "d": {
        "abs": 0.6,
        "perc": 0.4,
        'doseQt': 50,
        "tc": 0
    },
    "f": {
        "abs": 0.8,
        "perc": 0.05,
        'doseQt': 350,
    }
}
maxWeekly = 100
stop = False

totalAlchoolD = drinks["d"]["abs"] * drinks["d"]["perc"] 
totalAlchoolF = drinks["f"]["abs"] * drinks["f"]["perc"] 
maxMlInDestilled = maxWeekly / totalAlchoolD
maxMlInFermented = maxWeekly / totalAlchoolF

maxDosesDestilled = maxMlInDestilled / drinks["d"]["doseQt"]
maxCansFermented = maxMlInFermented / drinks["f"]["doseQt"]

print(f"Max Doses Destilled: {round(maxDosesDestilled)}, completing {round(maxMlInDestilled)} ml per week")
print(f"Max Cans Fermented: {round(maxCansFermented)}, completing {round(maxMlInFermented)} ml per week")