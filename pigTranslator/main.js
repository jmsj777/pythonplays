const { pigTranslator } = require('./translator.js')
const { test } = require('./test.js')

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
})

const handleLine = phrase => {
  if (phrase === 'exit') {
    readline.close()
    console.log('bye')
    return
  }
  const words = phrase.split(' ')
  let translations = words.map(word => pigTranslator(word))
  console.log(translations.join(' '))
  readline.question(`--> `, handleLine)
}

readline.question('Enter 1 to Run;\nEnter 2 to test\n--> ', res => {
  if (res == '1') readline.question(`--> `, handleLine)
  else if (res == '2') {
    test()
    handleLine('exit')
  } else {
    handleLine('exit')
  }
})
