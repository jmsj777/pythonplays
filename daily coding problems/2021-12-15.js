const hash = (lst, x) => {
  const firstPart = lst.filter(e => e < x)
  const secondPart = lst.filter(e => e === x)
  const thirdPart = lst.filter(e => e > x)
  return [...firstPart, ...secondPart, ...thirdPart]
}
hash([9, 12, 3, 5, 14, 10, 10], 10)
